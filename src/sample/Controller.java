package sample;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;

import java.net.InetAddress;

public class Controller {
    public Button button_1;
    public Button button_2;
    public Button button_3;
    public Button button_4;
    public Button button_5;
    public Button button_6;
    public Button button_7;
    public Button button_8;
    public Button button_9;
    public Button button_0;
    public Button button_add;
    public Button button_sub;
    public Button button_del;
    public Button button_mul;
    public Button button_div;
    public Button button_AC;
    public Button button_C;
    public Button button_sum;
    public Label monitor;
    public Label answer;
    public int c = 0, d = 0, add = 0, sub = 0, mul = 0, dev = 0, m = 0, n = 0, o = 0;
    public float a = 0, b = 0;
    public String s;
    public String[] str = new String[100];

    public void doClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String number = monitor.getText();
        if (c == 0) {
            monitor.setText(button.getText());
            str[m] = button.getText();
            c++;
            m++;
        } else {
            str[m] = button.getText();
            monitor.setText(number + button.getText());
            m++;
        }
    }

    public void doCompute(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                if (d == 0) {
                    a = Float.parseFloat(monitor.getText());
                    d++;
                    c = 0;
                    add = 1;
                    m = 0;
                } else if (d == 1) {
                    if (mul == 1) {
                        b = a * Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        mul = 0;
                        add = 1;
                        m = 0;
                    } else if (sub == 1) {
                        b = a - Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        sub = 0;
                        add = 1;
                        m = 0;
                    } else if (dev == 1) {
                        b = a / Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        dev = 0;
                        add = 1;
                        m = 0;
                    } else {
                        b = a + Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        add = 1;
                        m = 0;
                    }
                } else {
//-----------------------------------------------------------------------------------
                    if (mul == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b *= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        mul = 0;
                        add = 1;
                        m = 0;
                    } else if (sub == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b -= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        sub = 0;
                        add = 1;
                        m = 0;
                    } else if (dev == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b /= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        dev = 0;
                        add = 1;
                        m = 0;
                    } else {
                        a = Float.parseFloat(monitor.getText());
                        b += a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        add = 1;
                        m = 0;
                    }
                }
                break;
            case "-":
                if (d == 0) {
                    a = Float.parseFloat(monitor.getText());
                    d++;
                    c = 0;
                    sub = 1;
                    m = 0;
                } else if (d == 1) {
                    if (mul == 1) {
                        b = a * Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        mul = 0;
                        sub = 1;
                        m = 0;
                    } else if (add == 1) {
                        b = a + Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        add = 0;
                        sub = 1;
                        m = 0;
                    } else if (dev == 1) {
                        b = a / Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        dev = 0;
                        sub = 1;
                        m = 0;
                    } else {
                        b = a - Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        sub = 1;
                        m = 0;
                    }
                } else {
//-----------------------------------------------------------------------------------
                    if (mul == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b *= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        mul = 0;
                        sub = 1;
                        m = 0;
                    } else if (add == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b += a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        add = 0;
                        sub = 1;
                        m = 0;
                    } else if (dev == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b /= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        dev = 0;
                        sub = 1;
                        m = 0;
                    } else {
                        a = Float.parseFloat(monitor.getText());
                        b -= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        sub = 1;
                        m = 0;
                    }
                }
                break;
            case "*":
                if (d == 0) {
                    a = Float.parseFloat(monitor.getText());
                    d++;
                    c = 0;
                    mul = 1;
                    m = 0;
                } else if (d == 1) {
                    if (add == 1) {
                        b = a + Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        add = 0;
                        mul = 1;
                        m = 0;
                    } else if (sub == 1) {
                        b = a - Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        sub = 0;
                        mul = 1;
                        m = 0;
                    } else if (dev == 1) {
                        b = a / Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        dev = 0;
                        mul = 1;
                        m = 0;
                    } else {
                        b = a * Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        mul = 1;
                        m = 0;
                    }
                } else {
//-----------------------------------------------------------------------------------
                    if (add == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b += a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        add = 0;
                        mul = 1;
                        m = 0;
                    } else if (sub == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b -= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        sub = 0;
                        mul = 1;
                        m = 0;
                    } else if (dev == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b /= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        dev = 0;
                        mul = 1;
                        m = 0;
                    } else {
                        a = Float.parseFloat(monitor.getText());
                        b *= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        mul = 1;
                        m = 0;
                    }
                }
                break;
            case "/":
                if (d == 0) {
                    a = Float.parseFloat(monitor.getText());
                    d++;
                    c = 0;
                    dev = 1;
                    m = 0;
                } else if (d == 1) {
                    if (add == 1) {
                        b = a + Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        add = 0;
                        dev = 1;
                        m = 0;
                    } else if (sub == 1) {
                        b = a - Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        sub = 0;
                        dev = 1;
                        m = 0;
                    } else if (mul == 1) {
                        b = a * Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        mul = 0;
                        dev = 1;
                        m = 0;
                    } else {
                        b = a / Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d++;
                        c = 0;
                        dev = 1;
                        m = 0;
                    }
                } else {
//-----------------------------------------------------------------------------------
                    if (add == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b += a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        add = 0;
                        dev = 1;
                        m = 0;
                    } else if (sub == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b -= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        sub = 0;
                        dev = 1;
                        m = 0;
                    } else if (mul == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b *= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        mul = 0;
                        dev = 1;
                        m = 0;
                    } else {
                        a = Float.parseFloat(monitor.getText());
                        b /= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        dev = 1;
                        m = 0;
                    }
                }
                break;
            case "C":
                String number = "a";

                if (m > 1) {
                    m--;
                    for (n = 0; n < m; n++) {
                        if (o == 0) {
                            monitor.setText(str[n]);
                            number = monitor.getText();
                            o++;
                        } else {
                            str[m] = monitor.getText();
                            monitor.setText(number + str[n]);
                            number = monitor.getText();
                        }
                    }
                    o = 0;
                } else {
                    monitor.setText("0");
                    c = 0;
                }

                break;
            case "AC":
                a = 0;
                b = 0;
                c = 0;
                d = 0;
                m = 0;
                add = 0;
                sub = 0;
                mul = 0;
                dev = 0;
                monitor.setText("0");
                break;
            case "=":
                if (d == 1) {
                    if (add == 1) {
                        b = a + Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d = 0;
                        c = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else if (sub == 1) {
                        b = a - Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d = 0;
                        c = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else if (mul == 1) {
                        b = a * Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d = 0;
                        c = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else {
                        b = a / Float.parseFloat(monitor.getText());
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        d = 0;
                        c = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    }
                } else {
//-----------------------------------------------------------------------------------
                    if (add == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b += a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        d = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else if (sub == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b -= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        d = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else if (mul == 1) {
                        a = Float.parseFloat(monitor.getText());
                        b *= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        d = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    } else {
                        a = Float.parseFloat(monitor.getText());
                        b /= a;
                        s = String.valueOf(b);
                        if (s.indexOf(".") > 0) {

                            s = s.replaceAll("0+?$", "");

                            s = s.replaceAll("[.]$", "");

                        }
                        monitor.setText(s);
                        c = 0;
                        d = 0;
                        dev = 0;
                        add = 0;
                        sub = 0;
                        mul = 0;
                    }
                }
                m = 0;
                for (n = 0; n < monitor.getText().length(); n++) {
                    str[n] = monitor.getText().substring(n, n + 1);
                    m++;
                }
                break;
        }
    }
}
